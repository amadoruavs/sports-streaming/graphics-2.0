let dataJson = {
    "color": "#FF2626",
    "imagePath": "images/dublin.svg"
};

let state = 0;

function update(arg) {
    let newData;
    if ((typeof arg) === "string") {
        newData = JSON.parse(arg);
    } else {
        newData = arg;
    }
    dataJson = {
        ...dataJson,
        ...newData
    };
    readDataJson();
}


function play(arg) {
    state = 0.5;
    readDataJson();
    $(".logo-cont").css("display", "block")
    $(".touchdown").removeClass("touchdown-out")
    $(".touchdown").addClass("touchdown-in")
    setTimeout(() => {
        $(".touchdown-content").css("display", "flex")
        $(".touchdown-content").removeClass("content-out")
        $(".touchdown-content").addClass("content-in")
    }, 500);
}

function stop() {
    $(".touchdown-content").removeClass("content-in")
    $(".touchdown-content").addClass("content-out")
    setTimeout(() => {
        $(".touchdown").removeClass("touchdown-in")
        $(".touchdown").addClass("touchdown-out")
    }, 500)
    setTimeout(() => {
        $(".touchdown-content").css("display", "none")
        $(".logo-cont").css("display", "none")
        state = 1;
    }, 1000);
}

function next() {
}

function readDataJson() {
    $(".touchdown").css("background-color", dataJson["color"]);
    $(".team-logo").attr("src", dataJson["imagePath"])
}
