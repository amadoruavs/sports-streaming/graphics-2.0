let dataJson = {
    "homeName": "Amador",
    "homeColor": "#7B00A7",
    "homePossession": "1",
    "homeTimeouts": "3",
    "homeScore": "0",
    "awayName": "Dublin",
    "awayColor": "#FF2626",
    "awayPossession": "0",
    "awayTimeouts": "3",
    "awayScore": "0",
    "quarter": "Q1",
    "gameTime": "12:00",
    "playTime": "40",
    "down": "1st",
    "distance": "10"
};

let state = 0;

function update(arg) {
    let newData;
    if ((typeof arg) === "string") {
        newData = JSON.parse(arg);
    } else {
        newData = arg;
    }
    dataJson = {
        ...dataJson,
        ...newData
    };
    readDataJson();
}


function play(arg) {
    if (state == 1) {
        return;
    }
    state = 0.5;
    readDataJson();
    $("#logo").removeClass("logo-out");
    $("#logo").addClass("logo-in");
    $("#logo-cont").removeClass("scoreboard-out");
    $("#logo-cont").addClass("scoreboard-in");
    setTimeout(() => {
        state = 1;
    }, 1000);
}

function stop() {
    if (state == 0) {
        return;
    }
    state = 0.5;
    $("#logo").removeClass("logo-in");
    $("#logo").addClass("logo-out");
    $("#logo-cont").removeClass("scoreboard-in");
    $("#logo-cont").addClass("scoreboard-out");
    setTimeout(() => {
        state = 0;
    }, 1000);
}

function next() {
}

function parseBool(arg) {
    if (arg === "false" || arg === "0") {
        return false;
    } else if (!arg) {
        return false;
    } else {
        return true;
    }
}

function readDataJson() {
    $("#home-name").text(dataJson["homeName"])
    $("#home-section").css("background-color", dataJson["homeColor"])
    if (parseBool(dataJson["homePossession"])) {
        $("#home-possession").css("display", "inline-block");
    }
    else {
        $("#home-possession").css("display", "none");
    }
    if (!isNaN(parseInt(dataJson["homeTimeouts"]))) {
        homeTimeouts = parseInt(dataJson["homeTimeouts"]);
        for (let i = 1; i <= 3; i++) {
            $(`#home-timeout-${i}`).css("background-color", "#33333399");
        }
        for (let i = 1; i <= homeTimeouts; i++) {
            $(`#home-timeout-${i}`).css("background-color", "white");
        }
    }
    $("#home-score").text(dataJson["homeScore"])

    $("#away-name").text(dataJson["awayName"])
    $("#away-section").css("background-color", dataJson["awayColor"])
    if (parseBool(dataJson["awayPossession"])) {
        $("#away-possession").css("display", "inline-block");
    }
    else {
        $("#away-possession").css("display", "none");
    }
    if (!isNaN(parseInt(dataJson["awayTimeouts"]))) {
        awayTimeouts = parseInt(dataJson["awayTimeouts"]);
        for (let i = 1; i <= 3; i++) {
            $(`#away-timeout-${i}`).css("background-color", "#33333399");
        }
        for (let i = 1; i <= awayTimeouts; i++) {
            $(`#away-timeout-${i}`).css("background-color", "white");
        }
    }
    $("#away-score").text(dataJson["awayScore"]);

    $("#quarter").text(dataJson["quarter"]);
    $("#game-time").text(dataJson["gameTime"]);
    $("#play-time").text(dataJson["playTime"]);
    $("#down-distance").text(`${dataJson["down"]} & ${dataJson["distance"]}`);
}
