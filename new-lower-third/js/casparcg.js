let dataJson = {
    "homeImagePath": "images/amador.svg",
    "homeScore": "",
    "homeName": "Amador Valley Dons",
    "homeColor": "#8A00A5",
    "awayImagePath": "images/dublin.svg",
    "awayScore": "",
    "awayName": "Dublin Gaels",
    "awayColor": "#FF2626",
    "ltText": "The game will begin shortly."
};

let state = 0;

function update(arg) {
    let newData;
    if ((typeof arg) === "string"){
        newData = JSON.parse(arg);        
    } else {
        newData = arg;
    }
    dataJson = {
        ...dataJson,
        ...newData
    };
    readDataJson();
}

function play(arg) {
    if (state != 0){
        return;
    }
    state = 0.5;
    readDataJson();
    $("#vs-symbol").removeClass("vs-symbol-out");
    $("#vs-symbol").addClass("vs-symbol-in");
    setTimeout(() => {
        $("#vs-names").removeClass("vs-names-out");
        $("#vs-names").addClass("vs-names-in");
        $("#home-logo").removeClass("home-logo-out");
        $("#home-logo").addClass("home-logo-in");
        $("#away-logo").removeClass("away-logo-out");
        $("#away-logo").addClass("away-logo-in");
    }, 700);
    setTimeout(() => {
        $("#self-plug").removeClass("self-plug-out");
        $("#self-plug").addClass("self-plug-in");
        $(".lt-text-cont-cont").removeClass("lt-text-out");
        $(".lt-text-cont-cont").addClass("lt-text-in");
    }, 1700);
    setTimeout(() => {
        state = 1;
    }, 2700);
}

function stop() {
    if (state != 1) {
        return;
    }
    state = 0.5;
    $(".lt-text-cont-cont").removeClass("lt-text-in");
    $(".lt-text-cont-cont").addClass("lt-text-out");
    $("#self-plug").removeClass("self-plug-in");
    $("#self-plug").addClass("self-plug-out");
    setTimeout(() => {
        $("#vs-names").removeClass("vs-names-in");
        $("#vs-names").addClass("vs-names-out");
        $("#home-logo").removeClass("home-logo-in");
        $("#home-logo").addClass("home-logo-out");
        $("#away-logo").removeClass("away-logo-in");
        $("#away-logo").addClass("away-logo-out");
    }, 1000);
    setTimeout(() => {
        $("#vs-symbol").removeClass("vs-symbol-in");
        $("#vs-symbol").addClass("vs-symbol-out");    
    }, 2000);
    setTimeout(() => {
        state = 0;
    }, 2700);
}

function next() {
    
}

function readDataJson() {
    if (["images/acalanes.svg",
         "images/amador.svg", 
         "images/dublin.svg",
         "images/UAVs_logo.svg"
        ].includes(dataJson["homeImagePath"])){
            $("#home-image").attr("src", dataJson["homeImagePath"]);
        }
    if (["images/acalanes.svg",
         "images/amador.svg", 
         "images/dublin.svg",
         "images/UAVs_logo.svg"
        ].includes(dataJson["awayImagePath"])){
            $("#away-image").attr("src", dataJson["awayImagePath"]);
        }
    $("#home-score").text(dataJson["homeScore"])
    $("#away-score").text(dataJson["awayScore"])
    $("#home-name").text(dataJson["homeName"])
    $("#away-name").text(dataJson["awayName"])
    $("#home-logo").css("border-color", (dataJson["homeColor"] + "90"))
    $("#away-logo").css("border-color", (dataJson["awayColor"] + "90"))
    if (dataJson["ltText"]){
        $(".lt-text-cont-cont").removeClass("lt-text-in");
        $(".lt-text-cont-cont").removeClass("lt-text-out");
        $(".lt-text-cont-cont").css("display", "flex");
        $("#lt-text").text(dataJson["ltText"]);
    } else {
        $(".lt-text-cont-cont").css("display", "none");
    }
}